package jdbc;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Properties;


public class JdbcRoleDao extends AbstractjdbcDao implements RoleDao {
    private Properties propertiesJdbc;
    static final Logger logger = Logger.getLogger(JdbcUserDao.class);
    Properties propertiesLog = new Properties();
    InputStream is = getClass().getResourceAsStream("/log4j.properties");
    public JdbcRoleDao() {
//        PropertyConfigurator.configure("Servlet/src/main/resources/log4j.properties");
        try {
            propertiesLog.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            }
            catch (Exception e) {
                // ignore this exception
            }
        }
        PropertyConfigurator.configure(propertiesLog);
        propertiesJdbc = new Properties();
        try {
            propertiesJdbc.load(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("jdbc.properties"));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    final Connection createConnection() {
        return getConnection(propertiesJdbc.getProperty("db.driver"),
                propertiesJdbc.getProperty("db.url"),
                propertiesJdbc.getProperty("db.username"),
                propertiesJdbc.getProperty("db.password"));
    }

    static Connection getConnection(final String property,
                                    final String property2,
                                    final String property3,
                                    final String property4) {
        try {
            Class.forName(property);
            return DriverManager.getConnection(property2,
                    property3,
                    property4);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
            throw new RuntimeException();
        }
    }

    @Override
    public final void create(final Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("INSERT INTO ROLE VALUES(" + role.getId() + " , '"
                    + role.getName() + "')");
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public final void update(final Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("UPDATE ROLE SET NAME='" + role.getName()
                    + "' WHERE ID=" + role.getId());
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public final void remove(final Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DELETE FROM ROLE" + " WHERE ID=" + role.getId());
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public final Role findByName(final String name) {
        Role role = new Role();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM "
                     + "ROLE WHERE NAME='"
                     + name + "';")) {
            resultSet.next();
            initItems(role, resultSet);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new RuntimeException();
        }
        return role;
    }

    public final Role findByID(final long id) {
        Role role = new Role();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM "
                     + "ROLE WHERE ID='"
                     + id + "';")) {
            resultSet.next();
            initItems(role, resultSet);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new RuntimeException();
        }
        return role;
    }

    final void initItems(Role role,
                         final ResultSet resultSet) throws SQLException {
        if (resultSet.getRow() == 0) {
            role = null;
        } else {
            role.setId(resultSet.getLong("ID"));
            role.setName(resultSet.getString("NAME"));
        }
    }

}
