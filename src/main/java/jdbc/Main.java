package jdbc;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

public class Main {
    static final Logger logger = Logger.getLogger(Main.class);


    public static void main(String[] args) {
        Properties props = new Properties();
        InputStream is = new Main().getClass().getResourceAsStream("/log4j.properties");
//        PropertyConfigurator.configure("src/main/resources/log4j.properties");
        try {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            }
            catch (Exception e) {
                // ignore this exception
            }
        }
        PropertyConfigurator.configure(props);
//        final Long idUser = 6L;
//        User user = new User();
//        user.setId(1L);
//        user.setLogin("Pavel");
//        user.setPassword("1234");
//        user.setEmail("@Pavel");
//        user.setFirstName("Pavel");
//        user.setLastName("Voloshinov");
//        user.setBirthDay("05.12.1995");
//        User user1 = new User();
//        user1.setId(2L);
//        user1.setLogin("name1");
//        user1.setPassword("1235");
//        user1.setEmail("@name1");
//        user1.setFirstName("fname1");
//        user1.setLastName("lname1");
//        user1.setBirthDay("08.10.1994");
//        JdbcUserDao jud = new JdbcUserDao();

//        jud.create(user);
//        jud.create(user1);


//        user.setEmail("vs");
        //jud.update(user);

//        allUser = jud.findAll();
//        user1 = jud.findByLogin("sok");
//        user1 = jud.findByEmail("vs");
//
//        Role role = new Role();
//        Role role1 = new Role();
//        role.setId(1L);
//        role.setName("admin");
//        role1.setId(2L);
//        role1.setName("user");


//        jrd.create(role);
//        jrd.create(role1);

//        role1 = jrd.findByName(role.getNameRole());
//        jrd.remove(role);

//        String result = newDateFormat.format(date);
        logger.debug("Sample debug message");
        logger.debug("Sample debug message");
        logger.info("Sample info message");
        logger.warn("Sample warn message");
        logger.error("Sample error message");
        logger.fatal("Sample fatal message");

    }

}
